/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author meira Class used to simplify the process of write a text file.
 */
public class MyBufferedWriter {

    /**
     * output file
     */
    private File f;
    /**
     * output bufferedWriter
     */
    private BufferedWriter bw;

    /**
     * Constructor
     * @param f - file to write
     */
    public MyBufferedWriter(File f) {
        this.f = f;
        this.bw = Util.getBw(f);
    }

    

    /**
     * write a string in the file
     * @param str - string
     */
    public void write(String str) {
        try {
            bw.write(str);
        } catch (IOException ex) {
            Util.ioException(ex,f);
        }
    }

    /**
     * close de file
     */
    public void close() {
        try {
            this.bw.close();
        } catch (IOException ex) {
            Util.ioException(ex,f);
        }
    }
    
    

}
