/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 * @author meira Class used to keep a Cartesian coordinate in 2D.;
 */
public class Coordinate {

    /**
     * Point in R^2.
     */
    public final int x, y;

    /**
     * Create based in a line with three values as the DISPLAY_DATA_SECTION vrp
     * files. The line 3 5014 3585 represent id=3 and coordinate (5014,3585).
     *
     * @param line
     */
    public Coordinate(String line) {
        String list[] = line.split(" ");
        x = Integer.parseInt(list[1]);
        y = Integer.parseInt(list[2]);
    }
}
