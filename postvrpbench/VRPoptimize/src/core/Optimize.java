/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author meira This class made a very simple optimization based in swaps.
 */
public class Optimize {

    /**
     * The current best route
     */
    private Route bestRoute = null;
    /**
     * The instance
     */
    private Instance inst;
    /**
     * keyboard input
     */
    private Scanner keyboard;
    /**
     * File with instance descriptions.
     */
    private File instanceFile;

    /**
     * Empty constructor
     */
    public Optimize() {
        keyboard = new Scanner(System.in);
        instanceFile = new File("config/instances.txt");
    }

    /**
     * Optimize by swap.
     *
     * @param swapSize - size of the swap.
     * @param execTime - execution time in ms.
     */
    public void optimizeBySwap(int swapSize, int execTime) {

        //create a valid route.
        if (bestRoute == null) {
            bestRoute = new Route(inst);

            bestRoute.shuffle(inst.getMatrixSize());

        }

        System.out.printf("-----%d------\n", swapSize);
        Route other = new Route(inst);

        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() < start + execTime) {
            //shuffle swapSize elements
            other.shuffle(swapSize);

            if (other.aggregateCost() < bestRoute.aggregateCost()) {
                //update best
                bestRoute.setRoute(other);
                System.out.printf("swapSize %d %s\n", swapSize, bestRoute.costToString());
            } else {
                //back to original
                other.setRoute(bestRoute);
            }
        }
    }

    /**
     * Arbitrary condition used in this example.
     *
     * @param route - route
     * @return if is valid or not
     */
    /*public boolean valid(Route route) {
        int minNumberVehicles = inst.getNumberVehicles()/2 ;        
        boolean valid = route.numberOfVehicles() >= minNumberVehicles;        
        return valid;
    }*/
    /**
     * select the instance in the benchmark
     *
     * @return Line of the selected instance obtained in config/instances.txt
     */
    public String chooseInstance() {

        String inputLine = null;

        BufferedReader br = Util.getBr(instanceFile);

        try {
            //firt line is a message.
            String line = br.readLine();
            String list[] = line.split("\\.");
            //Print header after the first dot
            System.out.println(list[1].trim() + "\n");
            //the second line is a header
            br.readLine();

            System.out.println("Enter the instance ID (See " + instanceFile.getName() + ")");
            String id = keyboard.nextLine();
            //String id ="5";
            line = br.readLine();
            while (line != null && line.length() > 2) {
                if (line.startsWith(id + ";")) {
                    inputLine = line;
                }
                //System.out.println(in);
                line = br.readLine();
            }
        } catch (IOException ex) {
            Util.ioException(ex, instanceFile);
            System.exit(1);
        }
        if (inputLine == null) {
            System.out.println("ID not found");
            System.exit(0);
        }
        return inputLine;
    }

    /**
     * Create the instance.
     */
    public void createInstance() {
        String instanceLine = chooseInstance();
        System.out.println("");
        String list[] = instanceLine.split(";");
        File dir = new File(list[1]);
        if (!dir.exists()) {
            System.out.println("Not found:" + dir.getAbsolutePath());
            System.out.println("Check the directory and try again.");
            System.exit(0);
        }
        dir = new File(dir, list[2]);
        if (!dir.exists()) {
            System.out.println("Not found:" + dir.getAbsolutePath());
            System.out.println("Check the directory and try again.");
            System.exit(0);
        }
        File input = new File(dir, list[2] + ".vrp");
        if (!input.exists()) {
            System.out.println("Not found:" + input.getAbsolutePath());
            File zipped = new File(dir, list[2] + ".vrp.zip");
            if (zipped.exists()) {
                System.out.println("UNZIP " + zipped.getName());
                System.out.println("AND TRY AGAIN.");
            } else {

                System.out.println("Check the directory and try again.");
            }
            System.exit(0);
        }
        this.inst = new Instance(input);

    }

    /**
     * Execute the optimization.
     */
    public void optimize() {

        this.createInstance();

        System.out.println("The execution time is limited to k seconds. Enter k:");
        int execTime = Integer.parseInt(keyboard.nextLine().trim()) * 1000;

        //swap 4 element
        optimizeBySwap(4, execTime / 4);
        //swap 3 elements
        optimizeBySwap(3, execTime / 4);
        //swap 2 elements
        optimizeBySwap(2, execTime / 4);
        //swap 1 elements
        optimizeBySwap(1, execTime / 4);

    }

    /**
     * print the solution.
     */
    void printBestRoute() {
        this.bestRoute.writeFile();
        this.bestRoute.draw();
    }

    /**
     * Simple main class.
     *
     * @param args - nothing
     */
    public static void main(String args[]) {
        Optimize opt = new Optimize();
        opt.optimize();
        opt.printBestRoute();
    }

}
