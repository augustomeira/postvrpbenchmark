package core;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author meira. Class used to write a route in a given background image.
 */
public class Image {

    /**
     * The image
     */
    BufferedImage img;

    /**
     * Get the image width
     *
     * @return width
     */
    public int getWidth() {
        return img.getWidth();
    }

    /**
     * Get the image height
     *
     * @return height
     */
    public int getHeight() {
        return img.getHeight();
    }

    /**
     * Create an imagem object using the background image file.
     *
     * @param file - background image file.
     */
    public Image(File file) {
        try {
            this.img = ImageIO.read(file);
        } catch (IOException ex) {
            System.err.println("File not Found:" + file.getAbsolutePath());
            ex.printStackTrace();
            System.exit(1);

        }
    }

    /**
     * Write the current image in the output file.
     *
     * @param output - the output file.
     */
    public void WriteFile(File output) {
        try {
            ImageIO.write((BufferedImage) this.img, "png", output);
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
    }

    /**
     * Write a String in the image.
     *
     * @param text
     */
    public void writeString(String text) {

        int posX = getWidth() / 5;
        int posY = 10 * getHeight() / 12;
        int linesPread = getHeight() / 45;
        Graphics g = this.img.getGraphics();
        g.setColor(Color.black);
        //Font currentFont = g.getFont();
        //Font newFont = currentFont.deriveFont();
        Font newFont = new Font("Courier", Font.PLAIN, (int)(this.getHeight() * 0.02F));
        g.setFont(newFont);

        String list[] = text.split("\n");
        int lineCounter = 0;
        int columCounter = 0;

        Color bgColor = new Color(1f,1f,1f,.9f);

        for (String line : list) {
            int x = posX/30+ posX * columCounter;
            int y = posY + lineCounter * linesPread;
            FontMetrics fm = g.getFontMetrics();
            Rectangle2D rect = fm.getStringBounds(line, g);
            g.setColor(bgColor);            
            g.fillRect(x,
                    y - fm.getAscent(),
                    (int) rect.getWidth(),
                    (int) rect.getHeight());

            g.setColor(Color.black);
            g.drawString(line, x, y);
            if (lineCounter++ == 7) {
                lineCounter = 0;
                columCounter++;
            }
        }

    }

    /**
     * Draw a line segment between two coordinates.
     *
     * @param a - origin
     * @param b - destiny *
     */
    public void DrawSegment(Coordinate a, Coordinate b) {
        Graphics2D graf = (Graphics2D) this.img.getGraphics();
        graf.setStroke(new BasicStroke(getHeight() / 1500.0F));
        graf.setColor(Color.black);
        graf.drawLine(a.x, a.y, b.x, b.y);
    }
}
