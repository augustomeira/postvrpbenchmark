/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 *
 * @author meira Class used to save a route and to compute the objective
 * functions.
 */
public class Route {

    /**
     * the route
     */
    private ArrayList<Integer> mainRoute;
    /**
     * instance reference
     */
    private final Instance inst;
    /**
     * depot
     */
    public static final int DEPOT = 0;
    /**
     * Random generator
     */
    private Random rGen;
    /**
     * TimesStamp. Used to create file names.
     */
    String timesStamp;

    /**
     * * Create a simple route with n deliveries and k vehicles.
     *
     * @param inst the instance
     */
    public Route(Instance inst) {
        this.timesStamp = Util.timeStamp();
        rGen = new Random(1);
        this.inst = inst;
        int n = inst.getMatrixSize();
        int k = inst.getNumberVehicles();
        //se details in the main paper.
        this.mainRoute = new ArrayList(n + k - 1);
        for (int i = 1; i < n; i++) {
            this.mainRoute.add(i);
        }

        for (int i = 0; i < k - 1; i++) {
            this.mainRoute.add(DEPOT);
        }
    }

    /**
     * Copy the source route to destination. Origin is the parameter and
     * destination is "this".
     *
     *
     * @param other - origin route
     */
    public void setRoute(Route other) {

        this.mainRoute = (ArrayList<Integer>) other.mainRoute.clone();
    }

    /**
     * route random permutation. Exchange 2 random elements and repeat n times.
     *
     * @param n - number of pairs to be randomly exchanged.
     */
    public void shuffle(int n) {
        for (int i = 0; i < n; i++) {
            int a = rGen.nextInt(this.mainRoute.size());
            int b = rGen.nextInt(this.mainRoute.size());
            int auxA = mainRoute.get(a);
            int auxB = mainRoute.get(b);
            mainRoute.set(a, auxB);
            mainRoute.set(b, auxA);
        }

    }

    
    
    
    /**
     * The sum of the routes costs for all the vehicles.
     *
     * @return the value
     */
    public long sumAllRoutes() {
        return this.length(this.mainRoute);
    }

    /**
     * The number of vehicles. Empty routes are ignored.
     *
     * @return the value
     */
    public long numberOfVehicles() {
        ArrayList<ArrayList<Integer>> aux = split();
        return aux.size();
    }

    /**
     * Standard deviation between routes lengths. Normalization is used to avoid
     * non integer values. Let x be the original standard deviation. This method
     * computes x*x*k*k*(k-1) where k is the number of nonempty routes.
     *
     * @return the normalized standard deviation.
     */
    public BigInteger NormalizedStandardDeviation() {

        long k = numberOfVehicles();
        long sum = sumAllRoutes();
        BigInteger std = BigInteger.ZERO;
        ArrayList<ArrayList<Integer>> allRoutes = split();
        for (ArrayList<Integer> route : allRoutes) {
            long cost = this.length(route);
            BigInteger aux = BigInteger.valueOf(k * cost - sum);
            std = std.add(aux.multiply(aux));
        }
        return std;
    }

    /**
     * This method compute the standard deviation of a set o lengths. Empty
     * routes are ignored.
     *
     * @return the value
     */
    public double standardDeviation() {
        double k = numberOfVehicles();
        if (k == 1) {
            return 0;
        }
        double value = NormalizedStandardDeviation().doubleValue() / (k * k * (k - 1));
        return Math.sqrt(value);
    }

    /**
     * Compute the routes average length. Empty routes ares ignored.
     *
     * @return the value
     */
    public double averageRoute() {
        return this.sumAllRoutes() * 1.0 / this.numberOfVehicles();
    }

    /**
     * Split the original route in a set of routes. Every time a depot is found,
     * the route is closed and a new route is started. Empty routes are ignored.
     *
     * @return a set of Arraylists, one for each non empty route.
     */
    public ArrayList<ArrayList<Integer>> split() {
        ArrayList<ArrayList<Integer>> split = new ArrayList();
        ArrayList<Integer> aux = new ArrayList<>();

        for (int i = 0; i < this.mainRoute.size(); i++) {
            int delivery = this.mainRoute.get(i);
            if (delivery == DEPOT) {
                if (aux.size() > 0) {
                    split.add(aux);
                    aux = new ArrayList<>();
                }
            } else {
                aux.add(delivery);
            }
        }

        if (aux.size() > 0) {
            split.add(aux);
        }
        return split;
    }

    /**
     * Compute if a route is feasible with respect to the maxRoute constraint.
     *
     * @return true if and only if the route is feasible.
     */
    public int infeseability() {
        int infeseability = 0;
        ArrayList<ArrayList<Integer>> allRoutes = split();
        for (ArrayList<Integer> route : allRoutes) {
            long cost = this.length(route);
            if (cost > inst.getMaxRoute()) {
                infeseability++;
            }
        }
        return infeseability;
    }

    /**
     * Compute the length of a route. One additional edge is added between the
     * depot and the first element and between the depot and the last element.
     * The route can have repeated elements.
     *
     * @param route - the route
     * @return the route length
     */
    public long length(ArrayList<Integer> route) {
        assert (route.size() > 0);

        int first = route.get(0);
        int last = route.get(route.size() - 1);
        long cost = inst.getWeight(DEPOT, first) + inst.getWeight(DEPOT, last);
        for (int i = 0; i < route.size() - 1; i++) {
            int delivery1 = route.get(i);
            int delivery2 = route.get(i + 1);
            cost += inst.getWeight(delivery1, delivery2);
        }
        return cost;
    }

    /**
     * Convert the route to String. if printRouteElements is true, print also
     * the route elements.
     *
     * @param printRouteElements - print the elements if true.
     * @return the object converted to string.
     */
    public String toString(boolean printRouteElements) {
        String ret = "";

        long k = this.numberOfVehicles();
        double avg = inst.undoScale(this.sumAllRoutes() * 1.0 / k);
        double std = inst.undoScale(this.standardDeviation());
        String sum = inst.undoScaleStr(sumAllRoutes());

        ret += String.format("TotLen= %s\n", sum);
        ret += String.format("AvgLen= %f\n", avg);
        ret += String.format("Std   = %f\n", std);
        ret += String.format("Vehi  = %d\n", k);
        ret += String.format("Infea = %d\n", infeseability());
        ret += String.format("Cover = %s\n", this.coverAllElements());

        ret += "\n";
        ret += "\n";

        ArrayList<ArrayList<Integer>> allRoutes = split();

        if (printRouteElements) {
            ret += "Original: " + Util.arrayToString(mainRoute) + "\n";
        }

        int top = 0;

        ret += String.format("id cost        maxlen\n");

        for (ArrayList<Integer> route : allRoutes) {
            long cost = length(route);
            double length = length(route) * 1.0 / inst.getMaxRoute();
            ret += String.format("%2d %s %.2f\n", top++, inst.undoScaleStr(cost), length);
            if (printRouteElements) {
                ret += "ids " + Util.arrayToString(route) + "\n";
            }

        }

        return ret;
    }

    /**
     * Write this route in a text file.
     */
    public void writeFile() {
        File routeFile = new File(inst.getFile().getName().replace("\\.vrp", "") + timesStamp + ".txt");
        MyBufferedWriter bw = new MyBufferedWriter(routeFile);
        bw.write(this.toString(true));

        bw.close();
        System.out.println("Route saved at\n " + routeFile.getAbsolutePath());
    }

    /**
     * Such constraint always must be respected.
     *
     * @return true if the route cover all elements.
     */
    public boolean coverAllElements() {
        HashSet<Integer> hs = new HashSet<>();
        hs.add(DEPOT);
        for (Integer ele : this.mainRoute) {
            hs.add(ele);
        }
        return hs.size() == this.inst.getMatrixSize();
    }

    /**
     * Draw the route in a image file.
     */
    public void draw() {

        String fname = inst.getFile().getAbsolutePath();
        fname = fname + ".png";
        File background = new File(fname);
        Image img = new Image(background);
        int first = mainRoute.get(0);
        int last = mainRoute.get(mainRoute.size() - 1);
        drawSegment(DEPOT, first, img);
        drawSegment(last, DEPOT, img);
        for (int i = 0; i < mainRoute.size() - 1; i++) {
            int delivery1 = mainRoute.get(i);
            int delivery2 = mainRoute.get(i + 1);
            drawSegment(delivery1, delivery2, img);
        }

        String output = inst.getFile().getName().replace(".png", "") + timesStamp + ".png";
        File fout = new File(output);
        img.writeString(this.toString(false));
        img.WriteFile(fout);
        System.out.println("Route image saved\n " + fout.getAbsolutePath());

    }

    /**
     * Draw a segment between two give elements.
     *
     * @param indexA Index of the first element in the route.
     * @param indexB Index of the first element in the route.
     * @param img image
     */
    private void drawSegment(int indexA, int indexB, Image img) {
        Coordinate ca = inst.getPosition(indexA);
        Coordinate cb = inst.getPosition(indexB);
        img.DrawSegment(ca, cb);

    }

    /**
     * Convert the cost in a single string line.
     *
     * @return the cost converted to a string.
     */
    public String costToString() {
        long k = this.numberOfVehicles();
        double avg = inst.undoScale(this.sumAllRoutes() * 1.0 / k);
        double str = inst.undoScale(this.standardDeviation());
        return String.format("k %d avg %f std %f infeas %d", k, avg, str,infeseability());
    }
    
    /**
     * Arbitrary composed cost
     * @return 
     */
     public double aggregateCost(){
         
         int goal = this.inst.getNumberVehicles()/2;
         long dist = Math.abs(goal-numberOfVehicles())+1;
         
         
        return (4.1*this.sumAllRoutes()+1.2*this.standardDeviation())*dist;
        
    }

}
