package core;

import java.io.BufferedReader;
import java.io.File;
import java.util.HashMap;

/**
 *
 * @author Class used to parse an input file into a object.
 */
public class Instance {    
    /**
     * Weights.
     */
    private long w[][];
    /**
     * getMatrixSize. Number de vehicles+1.
     */
    private int matrixSize;
    /**
     * number of vehicles
     */
    private int k;
    /**
     * vector of coordinates
     */
    private Coordinate[] coordinate;
    /**
     * Input File
     */
    private File file;
    /**
     * Parameters map. Example TYPE : PostVRP. The value TYPE is the key and
     * PostVRP is the value in the map.
     */
    private HashMap<String, String> map;
    /**
     * Number of digits after the point. Example: 4.1223 has 4 digits after the
     * points.
     */
    private int digitsAfterPoint = -1;

    /**
     * max allowed route.
     */
    private long maxRoute;

    /**
     * Constructor
     *
     * @param f - instance input file.
     */
    public Instance(File f) {
        map = new HashMap<>();
        this.file = f;
        this.read();
    }

    /**
     * Load and parse the inpute file.
     */
    private void read() {

        BufferedReader br = Util.getBr(file);
        String line = Util.nextLine(br);
        while (line != null) {
            if (line.contains(":")) {
                String list[] = line.split(":");
                map.put(list[0].toUpperCase().trim(), list[1].trim());
            }
            if (line.equals("EDGE_WEIGHT_SECTION")) {
                readEdgeWeightSection(br);
            }
            if (line.equals("DISPLAY_DATA_SECTION")) {
                readDisplayDataSection(br);
            }
            //System.out.println(line);
            line = Util.nextLine(br);
        }

    }

    /**
     * Read the display data section
     *
     * @param br
     */
    private void readDisplayDataSection(BufferedReader br) {
        coordinate = new Coordinate[matrixSize];
        for (int i = 0; i < matrixSize; i++) {
            String line = Util.nextNonEmptyLine(br);
            coordinate[i] = new Coordinate(line);
        }
    }

    /**
     * Read the full Edge Weight Section
     *
     * @param br
     */
    private void readEdgeWeightSection(BufferedReader br) {

        int counter=0;
        String key = "DIMENSION";
        String value = this.get(key);
        matrixSize = Integer.parseInt(value) + 1;

        key = "MAX_VEHICLES";
        value = this.get(key);
        k = Integer.parseInt(value);

        key = "MAX_ALLOWED_ROUTE";
        value = this.get(key).trim();
        maxRoute = this.scale(value);

        w = new long[matrixSize][matrixSize];
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                value = Util.nextNonEmptyLine(br).trim();
                w[i][j] = this.scale(value);
                counter++;
                if(counter%10000000==0){
                    System.out.printf("%.0f%%\n",counter*100.0/(matrixSize*matrixSize));
                }
            }
        }
    }

    /**
     * Obtain the value of the parameter
     *
     * @param key - parameter name
     * @return the value.
     */
    private String get(String key) {
        return map.get(key);
    }

    /**
     * Obtain the weight between two elements
     *
     * @param i - first element
     * @param j - second element
     * @return the weight
     */
    long getWeight(int i, int j) {
        return w[i][j];
    }

    /**
     * Obtain the matrix size
     *
     * @return the value
     */
    public int getMatrixSize() {
        return matrixSize;
    }

    /**
     * Obtain the number of vehicles
     *
     * @return the value
     */
    public int getNumberVehicles() {
        return this.k;
    }

    /**
     * Obtain the max route
     *
     * @return the value
     */
    public long getMaxRoute() {
        return maxRoute;
    }

    /**
     * obtain the instance file
     *
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * Convert a rational number to a long. For instance. 78.1234 is converted
     * to 781234. The number of digits after the point must be constant.
     *
     * @param line - original rational number.
     * @return the associated long.
     */
    private long scale(String line) {

        String[] list = line.split("\\.");
        assert (list.length == 2);
        if (digitsAfterPoint == -1) {
            digitsAfterPoint = list[1].length();
        } else {
            if (digitsAfterPoint != list[1].length()) {
                System.out.println("\n\n\nInvalid precision for " + line);
                System.out.println("The correct number of digits after the point is " + digitsAfterPoint);
                System.out.println("Correct the input file and try again.");
                System.exit(0);
            }

        }
        return Long.parseLong(list[0] + list[1]);
    }

    /**
     * Convert the value back with respect to the function convertDoubleToLong.
     *
     * @param value - long
     * @return rational number
     */
    public String undoScaleStr(long value) {
        String format = "%0" + (digitsAfterPoint + 1) + "d";
        String aux = String.format(format, value);
        String ret = "";
        for (int i = 0; i < aux.length(); i++) {
            if (i == aux.length() - digitsAfterPoint) {
                ret += ".";
            }
            ret += aux.charAt(i);
        }
        //format
        while(ret.length()<13){
            ret=ret+" ";
        }
        return ret;
    }

    /**
     * Convert the value back with respect to the function convertDoubleToLong.
     *
     * @param value - long
     * @return rational number
     */
    public double undoScale(long value) {
        String str = this.undoScaleStr(value);
        return Double.parseDouble(str);
    }

    /**
     * Convert the value back with respect to the function convertDoubleToLong.
     *
     * @param value - double
     * @return rational number
     */
    public double undoScale(double value) {
        double newValue = value;
        for (int i = 0; i < digitsAfterPoint; i++) {
            newValue /= 10;
        }
        return newValue;
    }

    /**
     * The Coordinate associated to element
     *
     * @param id - element id
     * @return The coordinate
     */
    public Coordinate getPosition(int id) {
        return coordinate[id];
    }
    

    
}
