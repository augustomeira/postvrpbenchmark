/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import crateBenchmark.MD5CheckSum;
import java.io.File;

/**
 *
 * @author Guillhermm
 */
public class Export {

    //MyZippedWriter zos;
    File zoutput;
    String originalMD5;

    MyBufferedWriter bw;
    File output;

    public Export(File output, String md5) {
        this.output = output;
        this.originalMD5 = md5;
        this.zoutput = new File(output.getAbsolutePath() + ".zip");
        //zos = new MyZippedWriter(output);
        bw = new MyBufferedWriter(output);

    }

    public void add(String line) {
        //zos.write(line);
        bw.write(line);
    }

    public void writeFile() {

        //zos.close();
        bw.close();
        MD5CheckSum.getInstance().writeMD5file(output, originalMD5);
        //output.delete();
    }

}
