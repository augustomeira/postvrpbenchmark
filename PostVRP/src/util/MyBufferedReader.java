/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Guillhermm
 *
 * Loads a file into memory line by line into a string arraylist.
 */
public class MyBufferedReader {

    private BufferedReader bufferedReader = null;
    private File f;

    public MyBufferedReader(File f) {
        this.f = f;
        bufferedReader = Util.getBr(f);
    }

    public String nextLine() {
        String line = "";
        try {

            do {
                line = bufferedReader.readLine();
            } while (line != null && line.startsWith("#"));
        } catch (IOException ex) {
            Util.ioException(ex, f);
        }
        return line;
    }

    public void close() {
        try {
            this.bufferedReader.close();
        } catch (IOException ex) {
            Util.ioException(ex, f);
        }
    }

}
