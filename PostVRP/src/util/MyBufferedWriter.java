/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author meira
 */
public class MyBufferedWriter {

    private File f;
    private BufferedWriter bw;

    public MyBufferedWriter(File f) {
        this.f = f;
        this.bw = Util.getBw(f);
    }

    

    public void write(String str) {
        try {
            bw.write(str);
        } catch (IOException ex) {
            Util.ioException(ex,f);
        }
    }

    public void close() {
        try {
            this.bw.close();
        } catch (IOException ex) {
            Util.ioException(ex,f);
        }
    }
    public void flush(){
        try {
            this.bw.flush();
        } catch (IOException ex) {
            Util.ioException(ex,f);
        }
    }

}
