/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author meira
 */
public class Util {

    public static String getInstanceDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static BufferedWriter getBw(String fname) {
        File f = new File(fname);
        return getBw(f);
    }

    /**
     * Creates a buffered writer. Make a repetitive process automatic.
     *
     * @param fname - file getName
     * @return BufferedReader created
     */
    public static BufferedWriter getBw(File fname) {

        BufferedWriter out = null;
        FileOutputStream f = null;
        try {
            f = new FileOutputStream(fname);
            OutputStreamWriter fstream = new OutputStreamWriter(f, "UTF-8");
            out = new BufferedWriter(fstream);
        } catch (IOException ex) {

            System.out.println("Error to open the file for writing: " + fname.getAbsolutePath());
            System.out.println("Check the file and try again");
            ex.printStackTrace();
            System.exit(1);
        }
        return out;
    }

    public static BufferedWriter getBwz(File fname) {

        /*BufferedWriter out = null;
        ZipOutputStream f = null;
        try {
            //f = new ZipOutputStream(fname);
            //OutputStreamWriter fstream = new OutputStreamWriter(f, "UTF-8");
            //out = new ZipOutputStream(fstream);
        } catch (IOException ex) {
            
            System.out.println("Error to open the file for writing: " + fname.getAbsolutePath());
            System.out.println("Check the file and try again");
            ex.printStackTrace();
            System.exit(1);
        }/*/
        return null;
    }

    public static BufferedReader getBr(String fname) {
        File f = new File(fname);
        return getBr(f);        
    }
    
    public static BufferedReader getBr(File fname) {

        BufferedReader out = null;
        FileInputStream f = null;
        try {
            f = new FileInputStream(fname);
            InputStreamReader fstream = new InputStreamReader(f, "UTF-8");
            out = new BufferedReader(fstream);
        } catch (IOException ex) {

            System.out.println("Error to open the file for reading: " + fname.getAbsolutePath());
            System.out.println("Check the file and try again");
            //ex.printStackTrace();
            System.exit(1);
        }
        return out;
    }

    public static String timeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd_HHmm").format(Calendar.getInstance().getTime());
    }

    public static void ioException(Exception ex, File f) {
        ex.printStackTrace();
        System.out.println("Input/Output Problem " + f.getAbsolutePath());
        System.exit(-1);
    }

    public static void fileNotFoundException(FileNotFoundException ex, File f) {
        System.out.println("Error to open the file for reading: " + f.getAbsolutePath());
        System.out.println("Check the file and try again");
        System.exit(1);
    }

    public static void make(File path) {
        //File directory = new File(path);

        if (!(path.exists() && path.isDirectory())) {
            path.mkdir();
        }
    }
}
