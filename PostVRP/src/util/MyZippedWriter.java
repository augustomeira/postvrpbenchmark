/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author meira
 */
public class MyZippedWriter {

    private File f;
    ZipOutputStream bw;

    public MyZippedWriter(File fparam) {
        
        FileOutputStream fos=null;
        File f = new File(fparam.getAbsolutePath() + ".zip");
        try {            
            fos = new FileOutputStream(fparam.getAbsolutePath() + ".zip");
        } catch (FileNotFoundException ex) {
          Util.fileNotFoundException(ex, f);
        }
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        bw = new ZipOutputStream(bos);
        try {
            bw.putNextEntry(new ZipEntry(fparam.getName()));
        } catch (IOException ex) {
            Util.ioException(ex, fparam);
        }

    }

    public void write(String str) {
        
        try {
            bw.write(str.getBytes());
        } catch (IOException ex) {
            Util.ioException(ex, f);
        }
        
    }

    public void close() {
        try {
            this.bw.closeEntry();
            
            
            this.bw.close();
        } catch (IOException ex) {
            Util.ioException(ex, f);
        }
    }

    public void flush() {
        try {
            this.bw.flush();
        } catch (IOException ex) {
            Util.ioException(ex, f);
        }
    }
}
