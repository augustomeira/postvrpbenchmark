/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark;

import benchmark.image.Image;
import java.io.File;
import java.util.Locale;

import util.Util;

/**
 *
 * @author Gui
 */
public final class Parameters {

    
    public static String version = "1.02 (19/Oct/2019)";
    /**
     * singleton unique instance.
     */
    private static Parameters INST = null;
    private final File configDir =new File("config");
    private final File instanceFile = new File(configDir,"instances.txt");
    private final File backgroundFile=new File(configDir, "background.png");;
    private final File md5 = new File("log_"+Util.timeStamp()+"_.txt");;
    private final File streetsFile = new File("streets_"+Util.timeStamp()+"_.txt");;
    
    private final File modelFile = new File(configDir, "model.txt");
    private final int imgWidth;
    private final int imgHeight;
    
    
    
    //public static final boolean SAVE_IMAGES = true;
    public static final boolean EMPTY_MATRIX = false;

    /**
     * singleton pattern
     * @return the unique instance
     */

    public static Parameters getInstance() {
        if (null == INST) {
            INST = new Parameters();
        }
        return INST;
    }

    private Parameters() {
        Locale.setDefault(Locale.ENGLISH);
        

        if (!configDir.exists()) {
            System.out.println("VRPBench directory not found:\n" + configDir.getAbsolutePath());
            System.out.println("Please verify and try again.");
            System.exit(0);
        }
        
        if (!backgroundFile.exists()) {
            System.out.println("VRPBench background file not found:\n" + backgroundFile.getAbsolutePath());
            System.out.println("Please verify and try again.");
            System.exit(0);
        }
        
        if (!modelFile.exists()) {
            System.out.println("VRPBench model file not found:\n" + modelFile.getAbsolutePath());
            System.out.println("Please verify and try again.");
            System.exit(0);
        }

        Image img = new Image();
        img.newImg(backgroundFile.getAbsolutePath());
        this.imgHeight = img.getHeight();
        this.imgWidth = img.getWidth();

    }

    public String getBackgroundFile() {
        return this.backgroundFile.getAbsolutePath();
    }

    public int getWidth() {
        assert (imgWidth > 0);
        return imgWidth;
    }

    public int getHeight() {
        assert (imgHeight > 0);
        return imgHeight;
    }

    public File getModelFile() {
        return modelFile;
    }


    public File getInstanceFile() {
        return instanceFile;
    }

    public File getMd5File() {
                         
        return md5;
    }

    public File getStreetsFile() {
         
        return streetsFile;
    }
    
    
    
}


