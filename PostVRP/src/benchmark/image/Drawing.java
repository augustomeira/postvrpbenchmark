package benchmark.image;

import java.awt.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author meira
 */
public abstract class Drawing {

    protected int thickness = 1;
    protected Color color;

    //interface for objects printed on the image
    public abstract void Draw(Image i);

    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
