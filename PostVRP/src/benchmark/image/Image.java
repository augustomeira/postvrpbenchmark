/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.image;

import benchmark.graph.Edge;
import benchmark.graph.Graph;
import benchmark.graph.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author meira
 */
public class Image {

    BufferedImage img;

    public void DrawSquarePoint(int x, int y, int thickness, Color color) {
        for (int px = x - thickness / 2; px <= x + (thickness - 0.5) / 2; px++) {
            //Color c = new Color(color.red, color.green, color.blue);
            this.img.getGraphics().setColor(color);

            for (int py = y - thickness / 2; py <= y + (thickness - 0.5) / 2; py++) {
                if (px >= 0 && py >= 0 && px < this.img.getWidth() && py < this.img.getHeight()) {
                    try {
                        //this.img.setRGB(px, py, c.getRGB());
                        this.img.setRGB(px, py, color.getRGB());
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        int a = this.img.getHeight();
                        int b = this.img.getWidth();
                        System.out.println("test");
                        ex.printStackTrace();
                        System.exit(1);
                    }
                }
            }
        }
    }

    public void DrawCircularPoint(int x, int y, int radius, Color color) {
        int diameter = 2 * radius;
        Graphics2D g = (Graphics2D) this.img.getGraphics();

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(color);
        g.fillOval(x - radius, y - radius, diameter, diameter);
        g.setColor(color.darker());
        g.drawOval(x - radius, y - radius, diameter, diameter);

    }

    public void DrawSegment(int x1, int y1, int x2, int y2, int thickness, Color color) {
        for (int e = 0; e < thickness; e++) {
            //Color c = new Color(px.red, px.green, px.blue);
            Graphics graf = this.img.getGraphics();
            Graphics2D g = (Graphics2D) this.img.getGraphics();
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            /*
             graf.setColor(color);
             graf.drawLine(x1 + e, y1, x2 + e, y2);
             graf.drawLine(x1 - e, y1, x2 - e, y2);
             graf.drawLine(x1, y1 + e, x2, y2 + e);
             graf.drawLine(x1, y1 - e, x2, y2 - e);
             */
            float[] dash = { 4f, 4f, 4f };
            BasicStroke dashed
                    = new BasicStroke(1.0f,
                            BasicStroke.CAP_ROUND,
                            BasicStroke.JOIN_ROUND,
                            1f, dash, 2f);
            
            g.setStroke(dashed);
    
            g.setColor(color.darker());
            g.draw(new Line2D.Double(x1 + e, y1, x2 + e, y2));
            g.setColor(color);
            g.draw(new Line2D.Double(x1 - e, y1, x2 - e, y2));
            g.setColor(color.brighter());
            g.draw(new Line2D.Double(x1, y1 + e, x2, y2 + e));
            g.setColor(color.brighter());
            g.draw(new Line2D.Double(x1, y1 - e, x2, y2 - e));
        }
    }

    public int getWidth() {
        return img.getWidth();
    }

    public int getHeight() {
        return img.getHeight();
    }

    public void newImg(String fname) {
        File f = new File(fname);
        try {
            this.img = ImageIO.read(f);
        } catch (IOException ex) {
            System.err.println("File not Found:"+f.getAbsolutePath());
            ex.printStackTrace();
            System.exit(1);
            
        }
    }

    //@Override
    /*public void newImg(int dx, int dy) {
     this.img = new BufferedImage(dx, dy, BufferedImage.TYPE_INT_ARGB);
     Color c = new Color(255, 255, 255);

     for (int i = 0; i < dx; i++) {
     for (int j = 0; j < dy; j++) {
     this.img.setRGB(i, j, c.getRGB());
     }
     }
     }*/
    public void WriteFile(String fname) {
        try {
            File outputfile = new File(fname);
            ImageIO.write((BufferedImage) this.img, "png", outputfile);
            //System.out.println("Route at:\n " + outputfile.getAbsolutePath());
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
    }

    public void desenha(Graph g, double transp) {
        for (Edge e : g.getE()) {
            Color color = new Color(0, 120, 255, 230);
            Draw(e, color);
        }
        for (Vertex v : g.getV()) {
            Color color = new Color(0, 255, 20, 230);
            Draw(v, color);
        }
    }

    public void Draw(Edge e, Color color) {
        Vertex v1 = e.getStart();
        Vertex v2 = e.getEnd();
        this.DrawSegment(v1.getX(), v1.getY(), v2.getX(), v2.getY(), 2, color);
    }

    public void Draw(Coordinate v, Color color) {
        this.DrawCircularPoint(v.getX(), v.getY(), 2, color);
    }

}
