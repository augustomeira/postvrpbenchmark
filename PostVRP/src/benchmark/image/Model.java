/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.image;

import benchmark.Parameters;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import util.MyBufferedReader;

import util.MyBufferedWriter;

/**
 *
 * @author meira. Carrega as ruas. Calcula as esquinas e cria o grafo.
 */
public class Model {

    /**
     * file with the streets.
     */
    private final ArrayList<Street> streets = new ArrayList<>();        
    private static Model INST = null;
    private final Coordinate depot;
    HashMap<String, Double> define;
    

    public static Model getInstance() {
        if (null == INST) {
            INST = new Model();
        }
        return INST;
    }

    /**
     * Load the street file in the correct format.     
     *
     * @param fname - file with the streets.
     */
    private Model() {

        Parameters p = Parameters.getInstance();
        File f = p.getModelFile();

        MyBufferedWriter outputStreetFile = new MyBufferedWriter(Parameters.getInstance().getStreetsFile());

        outputStreetFile.write("St. Name; St. Probability Density;St. Cost to Cross (in pixel)\n");

        MyBufferedReader input = new MyBufferedReader(f);
      

        define = new HashMap<>();

        String line = input.nextLine();
        while (line!=null) {            
            if (isStreet(line)) {
                Street s = new Street(line, define);
                streets.add(s);
                outputStreetFile.write(s.toString());

            }
            if (line.startsWith("DEFINE")) {
                while (line.contains("  ")) {
                    line = line.replace("  ", " ");
                }
                String[] list = line.split(" ");                
                if (list.length != 3) {
                    System.out.println("Invalid format " + line + " at " + f.getAbsolutePath());
                    System.exit(1);
                }
                define.put(list[1], Double.parseDouble(list[2]));
            }
            line = input.nextLine();
        }
        input.close();

        this.createCorners();
        
        boolean ok = true;
        if (!define.containsKey("DEPOT_X")) {
            ok = false;
        }
        if (!define.containsKey("DEPOT_Y")) {
            ok = false;
        }
        if (!ok) {
            System.out.println("\n\n\n\nInvalid format. " + f.getAbsolutePath());
            System.out.println("You need to define depot position. See an example bellow.:");
            System.out.println("DEFINE DEPOT_X 600\n"
                    + "DEFINE DEPOT_Y 600");
            System.exit(0);

        }

        String aux[] = {"PIXEL_VALUE", "ADDITIONAL_COST_PER_DELIVERY", "DECIMAL_PRECISION"};

        for (String name : aux) {
            if (!define.containsKey(name)) {
                System.out.println("\n\n\n\nInvalid format. " + f.getAbsolutePath());
                System.out.println("You need to define " + name + ". See an example bellow.:");
                System.out.println("DEFINE " + name + " 10\n");
                System.exit(0);

            }
        }

        int depot_x = (int) define.get("DEPOT_X").doubleValue();
        int depot_y = (int) define.get("DEPOT_Y").doubleValue();
        depot = new Coordinate(depot_y, depot_x);
        outputStreetFile.close();
    }

    /**
     *
     * @param line
     * @return verdadeiro se é uma linha. Se for comentario ou vazia retorna
     * falso
     */
    private boolean isStreet(String line) {
        return line != null && line.length() > 4 && !line.contains("//") && line.contains("[") && !line.contains("DEPOT");
    }

    /**
     * Descobre todas as interseccoes de segmento e transforma numa esquina.
     */
    private void createCorners() {
        boolean alreadyHasCorner;
        //all streets in variable r1
        for (Street r1 : streets) {
            //all streets in variable r2
            for (Street r2 : streets) {
                alreadyHasCorner = false;
                // to not compare twice, compare only if the id is increasing
                if (r1.getID() < r2.getID()) {
                    //scan all segments of r1 in s1
                    for (SegmentCorner s1 : r1.getSegments()) {
                        if (alreadyHasCorner) {
                            continue;
                        }
                        //scan all segments of r2 in s2
                        for (SegmentCorner s2 : r2.getSegments()) {
                            //check for intersection (corner)
                            Coordinate x = s1.inter(s2);
                            if (x != null) {
                                if (!s1.updateExtreme(x)) {
                                    s1.addCorner(x);
                                }
                                if (!s2.updateExtreme(x)) {
                                    s2.addCorner(x);
                                }
                                alreadyHasCorner = true;
                            }
                        }
                    }
                }
            }
        }
    }

    public ArrayList<Street> getStreets() {
        ArrayList<Street> clone = new ArrayList<>();
        for (Street r : this.streets) {
            clone.add(r);
        }
        return clone;
    }

    @Override
    public String toString() {
        String ret = "";
        for (Street r : streets) {
            ret += r;
        }
        return ret;
    }

    

    public Street get(String nome) {
        Street ret = null;
        for (Street r : this.streets) {
            if (r.getName().equals(nome)) {
                if (ret != null) {
                    assert (false);
                    throw new RuntimeException("rua duplicada!\n");
                } else {
                    ret = r;
                }
            }
        }
        return ret;
    }

    public Coordinate getDepot() {
        return this.depot;
    }

    public double getParam(String key) {
        assert define != null : "define null";
        assert define.containsKey(key) : "Key não encontrada " + key;
        return define.get(key);
    }

    public void setParam(String key, String value) {
        Double d = new Double(value);
        this.define.put(key, d);
    }
    
    public double getAddCostByDelivery(){
        return getParam("ADDITIONAL_COST_PER_DELIVERY");
    }

}
