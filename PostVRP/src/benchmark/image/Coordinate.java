package benchmark.image;

import benchmark.Parameters;
import java.awt.Color;

/**
 *
 *
 * @author meira
 */
public class Coordinate extends Drawing {

    /**
     * position in Cartesian space.
     */
    protected int x, y;
    private final int standardThickness = 3;

    /**
     * constructor
     *
     * @param x - position x
     * @param y - position y
     * @param color - color 
     * @param thickness - thickness da impressao
     */
    public Coordinate(int x, int y, Color color, int thickness) {
        this.x = x;
        this.y = y;
        this.color = color;
        super.thickness = thickness;
    }

    /**
     * construtor
     *
     * @param x - position x
     * @param y - position y
     */
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
        super.thickness = standardThickness;

    }

    /**
     * @param str - rebe uma string num formato específico. Por exemplo
     * 2823,4357]- sigfica x=2823 e y=4357
     */
    public Coordinate(String str) {
        //System.out.println(str);
        str = str.replace("]", "").replace("#", "").replace("-", "").replace(" ", "").replace("\t", "");
        String list[] = str.split(",");
        this.y = Integer.parseInt(list[0]);
        this.x = Integer.parseInt(list[1]);
        boolean ok = true;
        Parameters p = Parameters.getInstance();
        if (this.x < 0 || this.x >= p.getWidth()) {
            ok = false;
        }
        if (this.y < 0 || this.y >= p.getHeight()) {
            ok = false;
        }
        if (!ok) {
            System.out.printf("The imagem maximum values are (width,height)=(%d,%d)\n", p.getWidth() - 1, p.getHeight() - 1);
            System.out.printf("The coordinate is invalid (%d,%d)\n", this.x, this.y);
            System.exit(0);

        }

        super.thickness = standardThickness;
    }

    public Coordinate combine(Coordinate other, double frac) {
        assert (frac <= 1);
        assert (frac >= 0);
        int x = (int) (this.x * (1.0 - frac) + other.x * frac);
        int y = (int) (this.y * (1.0 - frac) + other.y * frac);
        return new Coordinate(x, y);
    }

    /**
     *
     * @return - objeto convertido em string
     */
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    /**
     * position x
     *
     * @return
     */
    public int getX() {
        return this.x;
    }

    /**
     *
     * @return - position y
     */
    public int getY() {
        return this.y;
    }

    /**
     * Euclidean sitance
     *
     * @param o other coordinate
     * @return
     */
    public double dist(Coordinate o) {
        int dx = this.x - o.x;
        int dy = this.y - o.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public void sum(Coordinate other) {
        this.x += other.x;
        this.y += other.y;
    }

    @Override
    public void Draw(Image i) {
        //  i.DesenhaPonto(x, y, super.thickness, super.color);
        i.DrawCircularPoint(x, y, thickness, color);
    }

    public static Coordinate deviate(Coordinate ini, Coordinate fim, double width) {
        double dx = fim.x - ini.x;
        double dy = fim.y - ini.y;
        //perpendicular
        //Coordinate p = new Coordinate(-dy, dx);
        double len = Math.sqrt(dx * dx + dy * dy);
        int x = (int) (-dy * (width / len));
        int y = (int) (dx * (width / len));
        
        return new Coordinate(x, y);
        
    }

}
