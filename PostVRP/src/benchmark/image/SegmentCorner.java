/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.image;

/**
 *
 * @author meira - Segment with start, end and internal coordinates (corners)  
 * generated from intersection with other segments.
 */
public class SegmentCorner extends PlanarSegment {

    /**
     * corners
     */
    public Coordinate esq[] = new Coordinate[100];
    /**
     * number of corners
     */
    int top = 0;

    /**
     *
     * @param ini - segment start
     * @param fim- segment end
     */
    public SegmentCorner(Coordinate ini, Coordinate fim) {
        super(ini, fim);
    }

    private boolean has(Coordinate x) {
        Coordinate[] v = this.getCoordinate();
        for (int i = 0; i < v.length; i++) {
            if (v[i].dist(x) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Insert a corner in the segment.
     *
     * @param x
     */
    public void addCorner(Coordinate x) {
        if (!has(x)) {
            //insere a esquina num vetor ordenado.
            int var = top;
            while (var >= 1 && esq[var - 1].dist(start) > x.dist(start)) {
                esq[var] = esq[var - 1];
                var--;
            }
            esq[var] = x;
            top++;
        }
    }

    /**
     * If x is outside the segment, extend the segment until it reaches x.
    Suppose that x is collinear to the segment.
     *
     * @param x - new extreme
     * @return true if the extreme has been changed.
     */
    public boolean updateExtreme(Coordinate x) {

        if (x.dist(start) >= end.dist(start)) {
            end = x;
            return true;

        }
        if (x.dist(end) >= start.dist(end)) {
            start = x;
            return true;
        }
        return false;
    }

    /**
     *
     * @return - get all vertices, including start and end.
     */
    public Coordinate[] getCoordinate() {
        Coordinate c[] = new Coordinate[top + 2];
        c[0] = start;
        c[c.length - 1] = end;
        for (int i = 0; i < top; i++) {
            c[i + 1] = esq[i];
        }
        return c;
    }

    @Override
    public void Draw(Image img) {
        Coordinate[] v = this.getCoordinate();
        for (int i = 0; i < v.length - 1; i++) {
            PlanarSegment s = new PlanarSegment(v[i], v[i + 1], super.color, super.thickness);
            s.setColor(super.color);
            s.setThickness(super.thickness);
            s.Draw(img);
        }
    }

    public double length() {
        return super.start.dist(super.end);
    }
}
