package benchmark.image;

import java.util.HashMap;

/**
 *
 * @author meira
 */
public class Street extends Drawing {

    /**
     * incremental counter for ID generation
     */
    private static int objectCounter = 0;
    /**
     * unique incremetal identifier for Street
     */
    private final int id = objectCounter++;
    /**
     * name
     */
    private final String name;
    /**
     * * set of segments with internal points called corners
     */
    private final SegmentCorner seg[];
    /**
     * number of segments
     */
    private int top = 0;

    /**
     * probability density
     */
    private double probDensity = 1.0;

    //private boolean left_side;
    private double cross = -1;

    /**
     * Constructor... Get a street in a specific format.
     *
     * @param str -String - street defined by a similar pattern Rua Mart Nair
     * dos Santos Ferraz [4772,7071] - [4707,7085]
     *
     * First, the street name. Then, a set of coordinates. Each coordinate
     * inside brackets, in format [x,y]-[x2,y2].
     * @param define
     *
     *
     */
    public Street(String str, HashMap<String, Double> define) {
        //System.out.println(str);
        String list[] = str.split("\\[");
        name = list[0];
        seg = new SegmentCorner[list.length - 3];
        for (int i = 2; i < list.length - 1; i++) {
            Coordinate c1 = new Coordinate(list[i]);
            Coordinate c2 = new Coordinate(list[i + 1]);

            seg[i - 2] = new SegmentCorner(c1, c2);
            top++;
        }
        String aux = list[1].replace("]", "");
        list = aux.split(",");
        for (String listElement : list) {
            if (define.get(listElement) == null) {
                System.out.printf("Invalid line in input file. %s undefined\n line:%s", listElement, str);
                System.exit(0);
            } else {
                //System.out.printf("%s %s\n", listElement, define.get(listElement));
                this.probDensity *= define.get(listElement);
            }
            String key = listElement + "_CROSSCOST";
            /* if(listElement.equals("LARGEAVENUE")){
                System.out.println("...");
            }*/
            if (define.containsKey(key)) {
                if (cross < 0) {
                    cross = define.get(key);
                } else {
                    System.out.println("Ivalid Street:" + str);
                    System.out.printf("two costs to cross the street: not allowed. %.2f %.2f\n", cross, define.get(key));
                    System.exit(0);
                }
            }
        }

        if (cross < 0) {
            System.out.println("Ivalid Street:" + str);
            System.out.printf("undefined CROSS\n");
            System.exit(0);
        }
        /*else {
            System.out.println("x");
        }*/

    }

    /**
     * Convert the object to String
     *
     * @return
     */
    @Override
    public String toString() {
        String ret = String.format("%s;%.3f;%.1f", name, this.probDensity, cross);
        /*for (SegmentCorner segmentCorner : seg) {
            ret += segmentCorner;
        }*/
        return ret + "\n";
    }

    public SegmentCorner[] getSegments() {
        return this.seg;
    }

    public String getName() {
        return this.name;
    }

    public int getID() {
        return id;
    }

    @Override
    public void Draw(Image img) {
        assert (top > 0);
        for (int i = 0; i < top; i++) {
            seg[i].setColor(super.color);
            seg[i].setThickness(super.thickness);
            seg[i].Draw(img);
        }
    }

    int getNumeroMax() {
        throw new RuntimeException("Missing implementation");
    }

    public double getDensity() {
        return probDensity;
    }

    public double length() {
        double le = 0;
        for (SegmentCorner x : seg) {
            le += x.length();
        }
        return le;
    }

    public double getCross() {
        return cross;
    }

}
