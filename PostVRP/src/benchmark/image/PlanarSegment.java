/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.image;

import java.awt.Color;

/**
 *
 * @author meira - trecho de reta. Possui duas coordenadas, inicio e fim.
 */
public class PlanarSegment extends Drawing {

    /**
     * istart and end of the segment
     */
    protected Coordinate start, end;
    /**
     * variable used to join very close points
     */
    private static final int tol = 10;

    /**
     * Constructor
     *
     * @param start - Coordinate - segment start
     * @param end - Coordinate - segment end
     * @param color - segment color
     * @param thickness
     */
    public PlanarSegment(Coordinate start, Coordinate end, Color color, int thickness) {
        this.start = start;
        this.end = end;
        if (start.dist(end) == 0) {
            System.out.println("Error.");
        }
        assert (start.dist(end) > 0);
        //a = tang();
        //b = this.start.getY() - a * this.start.getX();
        super.color = color;
        super.thickness = thickness;
    }

    /**
     *
     * @param ini - segment start
     * @param fim -segment end
     */
    public PlanarSegment(Coordinate ini, Coordinate fim) {
        this(ini, fim, new Color(100, 100, 100), 3);
    }

    /**
     * Compute the intersection between segments
     *
     * @param other -Segment to compute intersection
     * @return Coordinate or null if the intersection do not exist
     */
    
    public Coordinate inter(PlanarSegment other) {
        //https://en.wikipedia.org/wiki/Line–line_intersection#Given_two_points_on_each_line
        double x1, y1;
        double x2, y2;
        double x3, y3;
        double x4, y4;

        x1 = this.start.getX();
        y1 = this.start.getY();
        x2 = this.end.getX();
        y2 = this.end.getY();

        x3 = other.start.getX();
        y3 = other.start.getY();
        x4 = other.end.getX();
        y4 = other.end.getY();
        
        //System.out.printf("(%.1f,%.1f)-(%.1f,%.1f) inter (%.1f,%.1f)-(%.1f,%.1f)\n",x1,y1,x2,y2,x3,y3,x4,y4);

        double delta = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

        //paralel
        if (Math.abs(delta) < 0.00001) {
            return null;
        }/*else{
            System.out.println("???");
        }*/
        double x_intersec = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        x_intersec = x_intersec / delta;
        double y_intersec = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);
        y_intersec = y_intersec / delta;

        PlanarSegment[] pt = {this, other};
        //verifico se o ponto gerado (x_intersec,y_intersec) está dentro dos
        //limites dos segmentos, com uma tolerancia de "tol" pixeis.
        for (int i = 0; i < 2; i++) {
            if ((x_intersec < pt[i].start.getX() - tol && x_intersec < pt[i].end.getX() - tol)
                    || (x_intersec > pt[i].start.getX() + tol && x_intersec > pt[i].end.getX() + tol)
                    || (y_intersec < pt[i].start.getY() - tol && y_intersec < pt[i].end.getY() - tol)
                    || (y_intersec > pt[i].start.getY() + tol && y_intersec > pt[i].end.getY() + tol)) {
                return null;
            }
        }
        return new Coordinate((int) x_intersec, (int) y_intersec);
    }

//    public Coordinate inter2(PlanarSegment s2) {
//        //se os segmentos são paralelos, não intersecção.
//        if (this.tang() == s2.tang()) {
//            return null; // não há intersecção
//        }
//        
//        double a = tang();
//        double b = this.start.getY() - a * this.start.getX();
//        
//        double s2a = s2.tang();
//        double s2b = s2.start.getY() - s2a * s2.start.getX();
//        
//        
//        //x interseccao. vem do sistema
//        //y=a1x+b1
//        //y=a2x+b2, isolando o x.
//        double x_intersec = (b - s2b) / (a - s2a) * -1.0;
//        //y: substitui x_intersec na equacao y=a1x+b.
//        double y_intersec = a * x_intersec + b;
//        //vetor com cois segmentos...
//        PlanarSegment[] pt = {this, s2};
//        //verifico se o ponto gerado (x_intersec,y_intersec) está dentro dos
//        //limites dos segmentos, com uma tolerancia de "tol" pixeis.
//        for (int i = 0; i < 2; i++) {
//            if ((x_intersec < pt[i].start.getX() - tol && x_intersec < pt[i].end.getX() - tol)
//                    || (x_intersec > pt[i].start.getX() + tol && x_intersec > pt[i].end.getX() + tol)
//                    || (y_intersec < pt[i].start.getY() - tol && y_intersec < pt[i].end.getY() - tol)
//                    || (y_intersec > pt[i].start.getY() + tol && y_intersec > pt[i].end.getY() + tol)) {
//                return null;
//            }
//        }
//        return new Coordinate((int) x_intersec, (int) y_intersec);
//    }

    /**
     *
     * @return distancia entre x1 e x2
     */
//    private double dx2() {
//        double dx = 1.0 * this.start.getX() - 1.0 * this.end.getX();
//        //se dx for zero, a reta é vertical e não pode ser escrita 
//        // na forma y=ax+b.
//        //nesse caso forcei dx=0.1
//        if (dx == 0) {
//            dx += 0.1;
//            //System.out.printf("Melhorar isso aqui....\n");
//        }
//        return dx;
//    }

    /**
     *
     * @return distancia entre y1 e y2
     */
//    private double dy() {
//        //converte para double.
//        return 1.0 * this.start.getY() - 1.0 * this.end.getY();
//    }

    /**
     *
     * @return coeficinete angular.
     */
    /*private double tang() {
        return this.dy() / this.dx();
    }*/

    /**
     *
     * @return objeto convertido para string.
     */
    @Override
    public String toString() {
        //return this.start + "---" + this.end + "  y=" + this.a + "x+" + this.b;
        return "";
    }

    @Override
    public void Draw(Image i) {
        int x1 = start.getX();
        int y1 = start.getY();
        int x2 = end.getX();
        int y2 = end.getY();
        //assert(x1<i.getWidth()&&x2<i.getWidth()):"x1="+x1+", x2="+x2+"largura="+i.getWidth();
        //assert(y1<i.getHeight()&&y2<i.getHeight()):"y1="+y1+", y2="+y2+"altura="+i.getHeight();
        //System.out.println(this);
        i.DrawSegment(x1, y1, x2, y2, super.thickness, super.color);
        i.DrawSquarePoint(x1, y1, super.thickness + 5, new Color(0, 0, 0));
        i.DrawSquarePoint(x2, y2, super.thickness + 5, new Color(0, 0, 0));

    }
}
