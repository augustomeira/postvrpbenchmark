/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.graph;

import java.io.Serializable;

/**
 *
 * @author meira
 */
public class AllPairShortestPath implements Serializable {

    private final int n = Graph.getInstance().n();
    double[][] dist = new double[n][n];
    int[][] next = new int[n][n];
    private final Vertex Predecessor[][] = new Vertex[n][n];
    private static final int MAX = Integer.MAX_VALUE / 1000;
    private static AllPairShortestPath INST = null;
    public static String fname = "arquivos/cache/AllPairShortestPath.bin";

    public static AllPairShortestPath getInstance() {
        if (null == INST) {

            INST = new AllPairShortestPath();

        }
        return INST;
    }

    private AllPairShortestPath() {
        init();
        computeDist();
    }

    private void init() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    dist[i][j] = MAX;
                } else {
                    dist[i][j] = 0;
                }
                next[i][j] = -1;
            }
        }

        for (Edge e : Graph.getInstance().getE()) {
            int u = e.getStart().id;
            int v = e.getEnd().id;
            dist[u][v] = dist[v][u] = e.getCost();
            next[u][v] = v;
            next[v][u] = u;
        }
    }

    private void computeDist() {
        for (int k = 0; k < n; k++) {
            
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (dist[i][k] != MAX
                            && dist[k][j] != MAX
                            && dist[i][j] > dist[i][k] + dist[k][j]) {
                        dist[i][j] = dist[i][k] + dist[k][j];
                        next[i][j] = next[i][k];
                    }
                }
            }
        }
    }

    public void print() {
        assert (false);
        System.out.println("W\n");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print("\t" + dist[i][j]);
            }

            System.out.println();
        }

    }

    public double getDist(Vertex v1, Vertex v2) {
        return this.dist[v1.id][v2.id];
    }

}
