/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.graph;

import benchmark.image.Coordinate;
import java.io.Serializable;

/**
 *
 * @author meira
 */
public class Vertex extends Coordinate implements Serializable {

    private static int top = 0;
    public final int id;

    public Vertex(int id, int x, int y) {
        super(x, y);
        this.id = id;
    }

    public Vertex(Coordinate x) {
        super(x.getX(), x.getY());
        this.id = top++;
    }

    @Override
    public String toString() {
        return this.id + " " + super.toString();
    }

    public boolean equals(Vertex other) {
        return (this.x == other.x) && (this.y == other.y);
    }

}
