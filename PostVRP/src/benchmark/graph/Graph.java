/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.graph;

import benchmark.image.Coordinate;
import benchmark.image.Street;
import benchmark.image.Model;
import benchmark.image.SegmentCorner;
import benchmark.image.Drawing;
import benchmark.image.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author meira
 */
public class Graph extends Drawing implements Serializable {

    ArrayList<Vertex> V;
    ArrayList<Edge> E;
    Edge[][] adj;
    public final int n;
    public final int m;
    private static final int tol = 2;
    private final boolean simple = true;

    public boolean isSimple() {
        return simple;
    }

    private static Graph INST = null;

    public static Graph getInstance() {
        if (null == INST) {

            INST = new Graph();

        }
        return INST;
    }

    /**
     * o grafo é criado a partir das ruas. coordenas próximas são colapsadas.
     */
    private Graph() {
        this.V = new ArrayList<>();
        this.E = new ArrayList<>();
        Vertex prev;
        Model rs = Model.getInstance();

        for (Street r : rs.getStreets()) {
            prev = null;
            double d = 0;

            for (SegmentCorner s : r.getSegments()) {
                Coordinate[] c = s.getCoordinate();

                for (Coordinate c1 : c) {
                    Vertex v = createVertex(c1);

                    if (prev != null && v != prev) {
                        Edge e = new Edge(prev, v, r.getName(), d, r);
                        d += v.dist(prev);
                        E.add(e);
                    }

                    prev = v;
                }
            }
        }

        this.n = this.V.size();

        for (int i = 0; i < n; i++) {
            Vertex v = this.V.get(i);
            //System.out.printf("V[%d].id=%d\n", i, V.get(i).id);
            assert (v.id == i);

        }

        this.m = this.E.size();
        this.adj = new Edge[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                adj[i][j] = null;
            }
        }

        for (Edge e : E) {
            int u = e.getStart().id;
            int v = e.getEnd().id;
            try {
                adj[u][v] = e;
                adj[v][u] = e;
            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.printStackTrace();
                System.exit(1);
            }

        }

        //double sum = this.sum();
        
        //double maxRoute1 =21599.76;
        //sum *=Model.getInstance().getParam("VALUE_OF_PIXEL");
        //System.out.printf("sum = %.3f",sum/maxRoute1);
        //System.exit(0);
        INST = this;
        
    }

    public Graph(String fname) {
        BufferedReader br = null;
        this.V = new ArrayList<>();
        this.E = new ArrayList<>();
        String line = null;
        try {
            br = new BufferedReader(new FileReader(new File(fname)));
            line = br.readLine();
            //System.out.println(linha);

        } catch (IOException ex) {
            Logger.getLogger(Graph.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }

        String[] lista = line.split(" ");
        this.n = Integer.parseInt(lista[0]);
        this.m = Integer.parseInt(lista[1]);
        this.adj = new Edge[n][n];
        try {
            for (int i = 0; i < n; i++) {
                line = br.readLine();
                String l2 = line.replace("(", ";").replace(",", ";").replace(")", ";").replace(" ", "");
                //System.out.print(linha+" --- "+l2+"\n");
                lista = l2.split(";");
                int id = Integer.parseInt(lista[0]);
                int x = Integer.parseInt(lista[1]);
                int y = Integer.parseInt(lista[2]);
                int z = Integer.parseInt(lista[3]);
                Vertex v = new Vertex(id, x, y);
                this.V.add(v);
                assert (this.V.get(v.id).id == id);
            }

            for (int i = 0; i < m; i++) {
                line = br.readLine();
                //System.out.println(line);
                String l2 = line.replace(") (", ";").replace(" (", ";").replace(") ", ";").replace(",", ";");
                //System.out.println(line + "   " + l2);
                lista = l2.split(";");
                String street = lista[0];
                int n_zero = Integer.parseInt(lista[1]);
                int start = Integer.parseInt(lista[2]);
                int end = Integer.parseInt(lista[3]);
                //remove s (second).
                double cost = Double.parseDouble(lista[4].replace("s", ""));
                assert (this.V.get(start).id == start);
                assert (this.V.get(end).id == end);
                Edge e = new Edge(this.V.get(start), this.V.get(end), street, n_zero, cost);
                this.E.add(e);
                assert (adj[start][end] == null);
                adj[start][end] = adj[end][start] = e;

            }
        } catch (IOException ex) {
            Logger.getLogger(Graph.class
                    .getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }

        INST = this;
        
    }

    /**
     * Create a new vertices. Coordinates closes are colapsed to a single
     * vertex.
     *
     * @param c1
     * @return
     */
    private Vertex createVertex(Coordinate c1) {
        for (Vertex v : V) {
            if (c1.dist(v) < tol) {
                return v;
            }
        }

        Vertex v = new Vertex(c1);
        V.add(v);
        return v;
    }

    public ArrayList<Vertex> getV() {
        return V;
    }

    public ArrayList<Edge> getE() {
        return E;
    }

    public int n() {
        return V.size();
    }

    public Edge get(Vertex u, Vertex v) {
        return adj[u.id][v.id];
    }

    public Edge get(int u, int v) {
        return adj[u][v];
    }

    public Vertex getVertice(int id) {
        assert (this.V.get(id).id == id);
        return this.V.get(id);
    }

    @Override
    public void Draw(Image i) {
        for (Edge e : this.E) {
            e.setColor(super.color);
            e.setThickness(super.thickness);
            e.Draw(i);
        }
    }
    private double sum(){
        double sum=0;
        for(Edge e: this.E){
            sum+=e.getCost();
        }
        return sum;
    }
    

}
