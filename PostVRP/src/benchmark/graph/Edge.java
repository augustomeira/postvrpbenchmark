/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.graph;

import benchmark.image.PlanarSegment;
import benchmark.image.Drawing;
import benchmark.image.Image;
import benchmark.image.Street;

import java.io.Serializable;

/**
 *
 * @author meira
 */
public class Edge extends Drawing implements Serializable {

    private Vertex start, end;
    private final String street;
    private final double n_zero;
    private double cost;
    private Object label = null;
    private final int id;
    private static int MAX = 0;
    //private static HashSet<String> set=new HashSet<>();

    public Edge(Vertex u, Vertex v, String street, double n_zero, Object label) {
        this.start = u;
        this.end = v;
        this.street = street;
        this.n_zero = n_zero;
        this.cost = u.dist(v);
        this.label = label;
        this.id = MAX++;
        //String key = u+":"+v;
        //if(set.contains(key)){
        //    assert(!set.contains(key));
        //}
        //set.add(key);
        
        
    }

    public double getCost() {
        return cost;
    }

    public Edge(Vertex u, Vertex v, String street, double n_zero, double cost, Object label) {
        this(u, v, street, n_zero, label);
        this.cost = cost;
    }

    public Vertex getStart() {
        return this.start;
    }

    public Vertex getEnd() {
        return this.end;
    }

    @Override
    public String toString() {
        return street + " (" + (int) this.n_zero + ") (" + this.start.id + "," + end.id + ") " + this.cost + "s";
    }

    public String address(double frac) {
        return "Street " + street + " n. " + (int) (this.n_zero + this.cost * frac);
    }

    public String getStreetName() {
        return this.street;
    }

    public double getNZero() {
        return this.n_zero;
    }

    @Override
    public void Draw(Image i) {
        PlanarSegment s = new PlanarSegment(start, end, super.color, super.thickness);
        s.Draw(i);
    }

    public double length() {
        return start.dist(end);
    }

    public Street getLabel() {
        return (Street) this.label;
    }

    public boolean equal(Edge other) {
        if (this.id == other.id) {
            return true;
        } else {
            boolean equalIni = this.start.equals(other.start);
            boolean euqalFim = this.end.equals(other.end);
            if (equalIni && euqalFim) {
                return true;
            }
            return false;
        }
    }

}
