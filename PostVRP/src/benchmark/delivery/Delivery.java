/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.delivery;

import benchmark.graph.AllPairShortestPath;
import benchmark.graph.Edge;
import benchmark.image.Coordinate;
import benchmark.image.Model;
import java.util.Random;

/**
 *
 * @author meira
 */
public class Delivery {

    /**
     * Edge
     */
    private final Edge e;
    /**
     * Give edge =(u,v) the delivery are in 
     * alpha*u+(1-alpha)v
     */
    private final double alpha;
    /**
     * side of delivery, left or right.
     */
    private final boolean left;    
    
    

    public Delivery(Edge e,Random r) {
        this.e = e;
        this.alpha = r.nextDouble();
        this.left = r.nextBoolean();
    }
    public Delivery(Edge e, double alpha, boolean isLeft) {
        this.e = e;
        this.alpha = alpha;
        this.left = isLeft;
    }

    public Edge getE() {
        return e;
    }

    public double getAlpha() {
        return alpha;
    }

    /*public boolean isLeft() {
        return left;
    }*/

    public double distance(Delivery other) {
        
        double distance;
        if (this.e.equal(other.e)) {
            distance = this.e.getCost() * Math.abs(this.alpha - other.alpha);            
        } else {
            AllPairShortestPath sp = AllPairShortestPath.getInstance();
            //start-start
            double v1 = sp.getDist(this.e.getStart(), other.getE().getStart());
            v1 += this.alpha * this.e.getCost() + other.alpha * other.e.getCost();
            distance = v1;
            //start-end
            double v2 = sp.getDist(this.e.getStart(), other.getE().getEnd());
            v2 += this.alpha * this.e.getCost() + (1.0 - other.alpha) * other.e.getCost();
            if (v2 < distance) {
                distance = v2;
            }
            //end-start
            double v3 = sp.getDist(this.e.getEnd(), other.getE().getStart());
            v3 += (1.0 - this.alpha) * this.e.getCost() + other.alpha * other.e.getCost();
            if (v3 < distance) {
                distance = v3;
            }
            //end-end
            double v4 = sp.getDist(this.e.getEnd(), other.getE().getEnd());
            v4 += (1.0 - this.alpha) * this.e.getCost() + (1.0 - other.alpha) * other.e.getCost();
            if (v4 < distance) {
                distance = v4;
            }

        }

        
        if(this!=other){
            distance+=Model.getInstance().getAddCostByDelivery();
        }
        
        return distance + cross(other);
    }

    public Coordinate getCoordinate() {
        Coordinate start = e.getStart();
        Coordinate end = e.getEnd();
        double wid=0;
        
        if(e.getLabel().getCross()>1.0){
            wid=e.getLabel().getCross()/2;
        }
        
        if (this.left) {
            wid = -wid;
        }
        
        Coordinate delta = Coordinate.deviate(start, end, wid);
        Coordinate pos = start.combine(end, alpha);
        pos.sum(delta);
        return pos;
    }

    public String toString() {
        Coordinate c = this.getCoordinate();
        return "(" + c.getX() + "," + c.getY() + ")\n";
    }


    public double cross(Delivery other) {  
        boolean sameSide =(this.left==other.left);
        boolean sameStreet = (this.e.getLabel().getID() == other.e.getLabel().getID());
        double value =  this.e.getLabel().getCross();
        if ((!sameSide)&&sameStreet) {
            return value;
        } else {
            return 0.0;
        }
    }

}
