/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.delivery;

import benchmark.graph.Edge;
import benchmark.graph.Graph;
import benchmark.image.Coordinate;
import benchmark.image.Model;
import java.util.ArrayList;

/**
 *
 * @author meira
 * 
 * Set of deliveries.
 */
public class Deliveries {

    public ArrayList<Delivery> dels = new ArrayList<>();

    public static Delivery depot = null;

    public Deliveries() {
        if (depot == null) {
            createDepot();
        }
        dels.add(depot);
    }

    /**
     * create a depot inside an edge, as close as possible to the depot location.
     */
    private void createDepot() {
        
        Graph g = Graph.getInstance();
        double minDist = Double.MAX_VALUE;
        Model sts = Model.getInstance();
        Coordinate depotCoor = sts.getDepot();
        for (Edge e : g.getE()) {
            for (double alpha = 0; alpha < 1; alpha += 0.001) {
                Coordinate combine = e.getStart().combine(e.getEnd(), alpha);
                double dist = combine.dist(depotCoor);
                if (dist < minDist) {
                    minDist = dist;
                    depot = new Delivery(e, alpha, true);
                }
            }
        }
    }

    public void addDelivery(Delivery d) {
        this.dels.add(d);
    }

    public int getSize() {
        return this.dels.size();
    }

    public Delivery get(int idx) {

        return dels.get(idx);

    }

}
