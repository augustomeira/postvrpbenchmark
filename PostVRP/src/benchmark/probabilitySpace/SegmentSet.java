/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark.probabilitySpace;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author meira
 *
 * Set of segments. Supose segments with weights 1,2 e 3.
 *
 * The non normalized segments will be: (---1---)(----2----)(-------3-------)
 *
 * The normalized segments will be:
 *
 * (-.17-)(---------.34-----)(--------.5-------)
 *
 * We need the beginning an the end of each segment.
 *
 */
public class SegmentSet {

    /**
     * list of segments
     */
    private final List<Segment> list;
    /**
     * accumulative probability of each segment, that represent the sart of the
     * segment
     */
    private double[] startOfSegment;
    /**
     * normalized (true/false).
     */
    boolean normilized = false;

    /**
     * Empty Constructor.
     */
    public SegmentSet() {
        list = new ArrayList<>();
    }

    /**
     * Add a segment.
     *
     * @param segment segment.
     */
    public void addSegment(Segment segment) {
        list.add(segment);
    }

    /**
     * Number of segments.
     *
     * @return the number of segments.
     */
    public int size() {
        return list.size();
    }

    /**
     * make the sum of the segment weights be 1.
     */
    public void normalize() {
        assert (normilized == false);
        normilized = true;
        double sum = 0;
        for (Segment e : list) {
            sum += e.getWeight();
        }
        for (Segment e : list) {
            e.normalize(sum);
        }
        startOfSegment = new double[list.size()];
        for (int i = 0; i < startOfSegment.length; i++) {
            startOfSegment[i] = this.getWeight(i);
        }
        for (int i = 1; i < startOfSegment.length; i++) {
            startOfSegment[i] += startOfSegment[i - 1];
        }
        for (int i = startOfSegment.length - 1; i > 0; i--) {
            startOfSegment[i] = startOfSegment[i - 1];
        }
        startOfSegment[0] = 0.0;
    }

    @Override
    public String toString() {
        String ret = "";
        for (int i = 0; i < list.size(); i++) {
            ret += list.get(i);
            if (startOfSegment != null) {
                ret += "" + String.format("%.2f", startOfSegment[i]) + "  ";
            }
        }
        return ret;
    }

    /**
     * Print the segments.
     */
    public void print() {
        System.out.println(this + "n");
    }

    /**
     * Get the weight of the segment.
     *
     * @param segIndex - segment Index
     * @return the weight of the segment.
     */
    public double getWeight(int segIndex) {
        return list.get(segIndex).getWeight();
    }

    /**
     * The sum of the weights of segments located before segIdx.
     *
     * @param segIdx
     * @return the start of the segment in the list.
     */
    public double getSegStart(int segIdx) {
        return startOfSegment[segIdx];
    }

    /**
     * The sum of the weights of segments located before segIdx plus the weight
     * of segIdx segment.
     *
     * @param segIdx - segment index.
     * @return the start of the segment in the list.
     */
    public double getSegmentEnd(int segIdx) {
        if (segIdx < list.size() - 1) {
            return startOfSegment[segIdx + 1];
        } else {
            return 1.0;
        }
    }

    /**
     * Obtain the segment
     *
     * @param segIdx segment index.
     * @return the segment.
     */
    public Segment get(int segIdx) {
        return list.get(segIdx);
    }

    public List<Segment> sample(int n, Random r) {
        //assert(normilized==false);
        ArrayList<Segment> sample = new ArrayList<>();

//        Random r =  Parameters.getInstance().getRgen();

        for (int entrID = 0; entrID < n; entrID++) {            
            double rand = r.nextDouble();
            for (int segID = 0; segID < this.size(); segID++) {
                if (rand >= this.getSegStart(segID) && rand < this.getSegmentEnd(segID)) {
                    Segment smp = this.get(segID);
                    sample.add(smp);
                }
            }
        }
        return sample;
    }

}
