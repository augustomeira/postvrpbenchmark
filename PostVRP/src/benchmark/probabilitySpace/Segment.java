package benchmark.probabilitySpace;

import benchmark.graph.Edge;

/**
 *
 * Segment contains weight and an id.
 *
 *
 */
public class Segment {

    /**
     * weight of the segment
     */
    private double weight;
    /**
     * unique Id.
     */
    private final int id;
    /**
     * static value used to create unique ids.
     */
    private static int MAX_ID = 0;
    /**
     * label
     */
    public Object label;

    public Segment(double density, double length, Object label) {
        this.weight = density * length;
        this.id = MAX_ID++;
        this.label = label;
    }

    /**
     * Get weight.
     *
     * @return the weight.
     */
    public double getWeight() {
        return this.weight;

    }

    @Override
    public String toString() {
        return "(" + id + "/" + String.format("%.2f", this.getWeight()) + ")";
    }

    /**
     * Obtain the ID.
     *
     * @return id.
     */
    public int getId() {
        return id;
    }

    /**
     * Divide the density by the value
     *
     * @param value
     */
    public void normalize(double value) {
        this.weight = this.weight / value;
    }

    public Edge getLabel() {
        return (Edge) label;
    }
}
