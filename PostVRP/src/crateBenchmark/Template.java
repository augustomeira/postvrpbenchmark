/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this outputFile file, choose Tools | Templates
 * and open the outputFile in the editor.
 */
package crateBenchmark;

import crateBenchmark.InputParameters;
import benchmark.Parameters;
import benchmark.delivery.Deliveries;
import benchmark.delivery.Delivery;
import benchmark.image.Coordinate;
import benchmark.image.Model;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import util.Export;

/**
 *
 * @author Gui
 */
public class Template {

    private final String instanceName;
    private final String instanceComment;
    private final String instanceDimension;
    private final String instanceNumberOfVehicles;
    private final String instanceMaxAllowedRoute;
    private final String instanceType;
    private final String instanceEdgeWeightType;
    private final String instanceEdgeWeightFormat;
    private final String instanceDisplayDataType;
    private final List<String> instanceDisplayDataSection;
    private final String instanceDepotSection;
    private final Deliveries deliveries;
    Export outputFile;
    
    
    String md5;
    

    public Template(InputParameters inputParameters, Deliveries dels, String name, File output,String md5) {
        assert(md5!=null);
        String emptyString = "";
        this.md5 = md5;
        instanceName = name;
        instanceComment = inputParameters.getCommentText();
        instanceType = "Distance-Constrained-VRP";
        instanceDimension = inputParameters.getNumberOfDeliveryPoints() + "";
        instanceNumberOfVehicles = inputParameters.getNumberOfVehicles() + "";
        
        instanceMaxAllowedRoute = convert(inputParameters.getMaxAllowedRoute());
        instanceEdgeWeightType = "EXPLICIT";
        instanceEdgeWeightFormat = "FULL_MATRIX";
        instanceDisplayDataType = "TWOD_DISPLAY";
        this.deliveries = dels;
        instanceDisplayDataSection = new ArrayList<>();
        instanceDisplayDataSection.add(emptyString);
        instanceDepotSection = "0\n-1\n";
        outputFile = new Export(output,this.md5);

    }

    protected void buildTemplate() {
        

        // Header
        outputFile.add("NAME : " + instanceName + "\n");
        outputFile.add("COMMENT : " + instanceComment + "\n");
        outputFile.add("TYPE : " + instanceType + "\n");
        outputFile.add("DIMENSION : " + instanceDimension + "\n");
        outputFile.add("MAX_VEHICLES : " + instanceNumberOfVehicles + "\n");
        outputFile.add("MAX_ALLOWED_ROUTE : " + instanceMaxAllowedRoute + "\n");
        outputFile.add("EDGE_WEIGHT_TYPE : " + instanceEdgeWeightType + "\n");
        outputFile.add("EDGE_WEIGHT_FORMAT : " + instanceEdgeWeightFormat + "\n");
        outputFile.add("DISPLAY_DATA_TYPE : " + instanceDisplayDataType + "\n");

        // Edge weight section
        outputFile.add("EDGE_WEIGHT_SECTION\n");

        int step=1000000;
        int stepTop=0;
        
        for (int line = 0; line < deliveries.getSize()&&!Parameters.EMPTY_MATRIX; line++) {
            //if(line%10==0){
                //System.out.printf("%d/%d\n",line,deliveries.getSize());
            //}
            for (int colum = 0; colum < deliveries.getSize(); colum++) {
                Delivery dLine = deliveries.get(line);
                Delivery dCol = deliveries.get(colum);
                                
                outputFile.add(convert(dLine.distance(dCol))+"\n");
                stepTop++;
                if(stepTop==step){
                    stepTop=0;
                    System.out.printf("%.1f%%\n",line*1.0/deliveries.getSize()*100);
                }
                
            }
            outputFile.add("\n");
            
        }

        // Display data section
        String aux = "";
        outputFile.add("DISPLAY_DATA_SECTION\n");
        for (int line = 0; line < deliveries.getSize(); line++) {
            Coordinate coo = deliveries.get(line).getCoordinate();
            aux = String.format("%d %d %d\n", line , coo.getX(), coo.getY());
            outputFile.add(aux);
        }

        // Depot section
        outputFile.add("DEPOT_SECTION\n");

        outputFile.add(this.instanceDepotSection + "\n");

        // End of File
        outputFile.add("EOF");
    }

    /*protected List<String> getTemplate() {
        return outputFile;
    }*/

    private String convert(double v){
        v *= Model.getInstance().getParam("PIXEL_VALUE");
        int prec = (int)(Model.getInstance().getParam("DECIMAL_PRECISION"));
        String aux = "%."+prec+"f";
        return String.format(aux, v).replace(",", ".");
    }
    
    public void writeFile() {
        this.buildTemplate();
        this.outputFile.writeFile();
    }
}
