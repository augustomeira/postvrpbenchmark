/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crateBenchmark;

import benchmark.Parameters;
import benchmark.image.Model;

/**
 *
 * @author meira
 */
public class createBenchmark {

    
    
    public static void main(String args[]) {

        System.out.println("\nVRPBench: A Vehicle Routing Benchmark Tool "+Parameters.version);
        System.out.println("Luis A. A. Meira, Guilherme A. Zeni, Mauro Menzori, P. S. Martins \n");
        //System.out.println(");
        
        Parameters.getInstance();
        Model.getInstance();
        AllInstances.getInstance();
        MD5CheckSum.getInstance().close();
        //System.out.println("");

    }
}
