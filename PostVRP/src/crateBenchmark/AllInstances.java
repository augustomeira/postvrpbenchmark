/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crateBenchmark;

import benchmark.Parameters;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Util;

/**
 *
 * @author meira
 */
public class AllInstances {

    private static AllInstances inst = null;
    private Parameters p = Parameters.getInstance();
    private ArrayList<Instance> list = new ArrayList<>();

    public static AllInstances getInstance() {
        if (inst == null) {
            inst = new AllInstances();
        }
        return inst;
    }

    private AllInstances() {
        readInstances();

    }

    public void readInstances() {
        //System.out.println("");
        BufferedReader br = Util.getBr(p.getInstanceFile());
        try {
            //firt line is a message.
            String line = br.readLine();
            System.out.println(line+"\n");
            //the second line is a header
            line = br.readLine();
            boolean ok = false;
            Scanner teclado = new Scanner(System.in);
            System.out.println("Enter the instance ID or -1 for all instances.\nSee "+p.getInstanceFile().getAbsolutePath()+"");
            String id = teclado.nextLine();
            //String id = "5";
            line = br.readLine();
            while (line != null && line.length() > 2) {

                
                if(id.equals("-1")||line.startsWith(id+";")){
                    Instance in = new Instance(line);
                    list.add(in);
                    ok = true;
                }
                
                
                //System.out.println(line);

                line = br.readLine();
            }
            if(!ok){
                System.out.println("ID not found.");
            }
        } catch (IOException ex) {
            Logger.getLogger(AllInstances.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }

    }

}
