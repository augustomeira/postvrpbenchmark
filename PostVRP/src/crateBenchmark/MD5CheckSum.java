/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crateBenchmark;

/**
 *
 * @author meira
 */
import benchmark.Parameters;
import java.io.*;
import java.lang.reflect.Parameter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.MyBufferedWriter;
import util.Util;

public class MD5CheckSum {

    private static MD5CheckSum inst = null;
    public MyBufferedWriter outputLog;
    String md5 = "";

    public static MD5CheckSum getInstance() {
        if (inst == null) {
            inst = new MD5CheckSum();
        }
        return inst;
    }

    private MD5CheckSum() {
        outputLog = new MyBufferedWriter(Parameters.getInstance().getMd5File());
    }

    public byte[] createCheckSum(File filename) {
        try {
            InputStream fis = new FileInputStream(filename);

            byte[] buffer = new byte[1024];
            MessageDigest complete = MessageDigest.getInstance("MD5");
            int numRead;

            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            return complete.digest();
        } catch (NoSuchAlgorithmException | IOException ex) {
            Logger.getLogger(MD5CheckSum.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        return null;
    }

    public void computeMD5(File file) {
        byte[] b = createCheckSum(file);
        String result = "";

        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        this.md5 = result;
    }

    public String updateExtension(File f) {
        String[] list = f.getAbsolutePath().split("\\.");
        String fname = "";
        for (int i = 0; i < list.length - 1; i++) {
            fname += list[i] + ".";
        }
        return fname + "md5";

    }

    public String writeMD5file(File file, String original) {

        computeMD5(file);

        assert (md5 != null);
        assert (original != null);

        String message;
        if (original.trim().equals(md5.trim())) {
            message = "(success)";
        } else {
            message = "Problem. Original md5 " + original;
        }

        File dest = new File(updateExtension(file));
        MyBufferedWriter bw = new MyBufferedWriter(dest);
        String str = String.format("MD5 (%s) %s  %s %s\n", file.getName(), md5, message, Util.timeStamp());
        bw.write(str);
        bw.close();

        System.out.print(str);

        //str = str.replace("\n", " at "+timeStamp);
        //str = "Local "+str;
        outputLog.write(str);
        outputLog.flush();

        return md5;

    }

    public void close() {
        outputLog.close();

    }

    public static void main(String args[]) {

        File f = new File("/Users/meira/dpboxfora/VRPBenchmark/RealWorldPost/toy/RealWorldPostToy_3_0/RealWorldPostToy_3_0.vrp.zip");

        //MD5CheckSum.getInstance().writeMD5file(f);
        //MD5CheckSum.getInstance().close();
    }

}
