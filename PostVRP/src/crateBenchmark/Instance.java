/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crateBenchmark;

import benchmark.Parameters;
import benchmark.delivery.Deliveries;
import benchmark.delivery.Delivery;
import benchmark.graph.Edge;
import benchmark.graph.Graph;
import benchmark.image.Coordinate;
import benchmark.image.Image;
import benchmark.probabilitySpace.Segment;
import benchmark.probabilitySpace.SegmentSet;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import util.Util;

/**
 *
 * @author meira
 */
public class Instance {

    private String fname;
    private File dir;
    private static SegmentSet segments = null;
    InputParameters inputPar = new InputParameters();
    String md5;
    Random r;
    int id;

    public Instance(String line) {
        if (segments == null) {
            createSegments();
        }

        String[] list = line.split(";");
        this.id = Integer.parseInt(list[0]);
        Util.make(new File(list[1]));
        dir = new File(list[1] + "//" + list[2]);
        Util.make(dir);
        fname = list[2];
        inputPar.setNumberOfDeliveryPoints(Integer.parseInt(list[3].trim()));
        inputPar.setNumberOfVehicles(Integer.parseInt(list[4].trim()));
        inputPar.setMaxAllowedRoute(Double.parseDouble(list[5].trim()));
        inputPar.setCommentText(list[6].trim());
        inputPar.setImageGenerated(true);
        inputPar.setNumberOfInstances(1);
        int seed = Integer.parseInt(list[7].trim());
        r = new Random(seed);
        md5 = list[8].trim();
        assert (md5 != null);
        this.createInstance();

    }

    public String toString() {
        String r = "----\n";
        r += String.format("%s\n", dir.getAbsolutePath());
        r += String.format("%s d=%d k=%d maxR=%.4f\n", fname, inputPar.getNumberOfDeliveryPoints(),
                inputPar.getNumberOfVehicles(), inputPar.getMaxAllowedRoute());
        r += String.format("%s\n----", inputPar.getCommentText());
        return r;
    }

    private void createInstance() {

        File file = new File(dir, fname + ".vrp");
        Deliveries deliveries = createDeliveryPoints(inputPar.getNumberOfDeliveryPoints());

        Template template = new Template(inputPar, deliveries, fname, file, md5);
        template.writeFile();
        if (inputPar.isImageGeneratable()) {
            generateInstancesImages(deliveries, file.getAbsolutePath());
        }

    }

    public Deliveries createDeliveryPoints(int n) {

        Deliveries inst = new Deliveries();
        List<Segment> sample = this.segments.sample(n, r);
        //boolean side = true;
        for (Segment seg : sample) {
            Delivery d = new Delivery(seg.getLabel(), r);
            //Delivery d = new Delivery(seg.getLabel(), 0, side);
            //side = !side;
            inst.addDelivery(d);
        }

        return inst;
    }

    private void generateInstancesImages(Deliveries deliveries, String path) {
        Image img = new Image();
        img.newImg(Parameters.getInstance().getBackgroundFile());

        List<String> dataSection = new ArrayList<>();

        Graph g = Graph.getInstance();

        img.desenha(g, 0.9);

        int radius = 10;
        Color color = new Color(0, 255, 20);

        Coordinate depot = deliveries.get(0).getCoordinate();
        img.DrawCircularPoint(depot.getX(), depot.getY(), radius, color);

        radius = 3;
        color = new Color(0, 0, 220);

        for (int idx = 1; idx < deliveries.getSize(); idx++) {
            Delivery del = deliveries.get(idx);
            img.DrawCircularPoint(del.getCoordinate().getX(), del.getCoordinate().getY(), radius, color);
            //System.out.println(del);
        }

        img.WriteFile(path + ".png");

        //return dataSection;*/
    }

    /**
     * Create the segments from based in the input file. The length is
     * proportional to the street length and the density is arbitrarly defined.
     *
     */
    private static void createSegments() {
        segments = new SegmentSet();
        Graph g = Graph.getInstance();
        ArrayList<Edge> edges = g.getE();
        for (Edge e : edges) {
            Segment seg = new Segment(e.length(), e.getLabel().getDensity(), e);
            segments.addSegment(seg);
        }
        segments.normalize();
        //segments.print();
    }
}
