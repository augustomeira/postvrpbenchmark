/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crateBenchmark;

/**
 *
 * This class contains parameters to create instances.
 */
public class InputParameters {

    private int numberOfDeliveries, numberOfInstances, numberOfVehicles;
    private String comment;
    private boolean generateImages=false;
    private double maxRoute;

    public void setCommentText(String comment) {
        this.comment = comment;
    }

    public void setImageGenerated(boolean generateImages) {
        this.generateImages = generateImages;
    }

    public void setMaxAllowedRoute(double maxRoute) {
        this.maxRoute = maxRoute;
    }
    
    /**
     * Set delivery Points number
     *
     * @param deliveryPointsNumber
     */
    public void setNumberOfDeliveryPoints(int deliveryPointsNumber) {
        this.numberOfDeliveries = deliveryPointsNumber;
    }

    /**
     * Set number of instances
     *
     * @param instances
     */
    public void setNumberOfInstances(int instances) {
        this.numberOfInstances = instances;
    }

    /**
     * Set number of vehicles
     *
     * @param vehicles
     */
    public void setNumberOfVehicles(int vehicles) {
        this.numberOfVehicles = vehicles;
    }

    public String getCommentText() {
        return this.comment;
    }

    public double getMaxAllowedRoute() {
        return this.maxRoute;
    }
    
    public int getNumberOfDeliveryPoints() {
        return this.numberOfDeliveries;
    }

    public int getNumberOfInstances() {
        return this.numberOfInstances;
    }

    public int getNumberOfVehicles() {
        return this.numberOfVehicles;
    }

    public boolean isImageGeneratable() {
        return this.generateImages;
    }

}
