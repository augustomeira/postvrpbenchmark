# PostVRP Benchmark #
* * *

This repository provides the source code for the PostVRP Benchmark, developed in Java. It is possible to use the benchmark tool to generate your own instances and ensure their identity thanks to the MD5 signature. Then, the researchers can apply their own mechanisms to optimize the instances and visualize the solutions. 

## DIRECTORIES ##

### PostVRP ###

Directory containing the project to generate the benchmark.

### postvrpbench ###

Directory available at the [project site](http://www.ft.unicamp.br/docentes/meira/postvrp).

### postvrpbench/VRPBench ###

The benchmark configuration files.

### postvrpbench/VRPoptimize ###

Toy optimizer.

## CONTACT ##

Please, if there are any *bugs*, comments, suggestions, contact [Luis A. A. Meira](mailto:meira@ft.unicamp.br).
